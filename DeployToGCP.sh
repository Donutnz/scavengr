#!/bin/bash

#Deploy app to GCP
#Would usually use BB pipeline but bad deploy chewed up all minutes.

PROJECT_NAME="scavengr"
CONFIG="Release"
GCLOUD_PROJECT="scavengr-214300"

echo "Restoring..."
dotnet restore $PROJECT_NAME
echo "Done"
echo "Building..."
dotnet build -c $CONFIG $PROJECT_NAME
echo "Done"
echo "Publishing..."
dotnet publish -c $CONFIG $PROJECT_NAME
echo "Done"

echo "Deploying..."
gcloud config set project $GCLOUD_PROJECT
gcloud app deploy --verbosity=debug --stop-previous-version --promote ./$PROJECT_NAME/bin/$CONFIG/netcoreapp2.1/publish/app.yaml
echo "Done"
