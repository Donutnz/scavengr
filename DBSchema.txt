Schema draft:
USER(~UserID, Email, Username, Reputation, Verified, Created, LastLogin, UserFlags)
VOTE(~UserID, ~PostID, UpVote)
FLAGS(~UserID, ~PostID, Flags)
POST(~PostID, Title, Description, Tags, Status, Date, PostFlags, *UserID, *ImgID, *MarkID)
IMAGE(~ImgID, Path)
MARKER(~MarkID, Coordinate)
