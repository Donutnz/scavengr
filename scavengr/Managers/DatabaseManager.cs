﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using scavengr.DataObjects;
using scavengr.Models;

namespace scavengr.Managers {
	/// <summary>
	/// Handle messing with DB. Might make static.
	/// </summary>
	public class DatabaseManager {
		private static string url = "35.240.135.15";
		private static string dbUser = "scavengr";
		private static string dbPass = "FearTheKnifeKing";
		//private static string dbUser = "root";
		//private static string dbPass = "TheTerribleTruce";
		private static string connStr {
			get {
				return String.Format("Server={0};Database=Scavengr;Uid={1};Password={2};Allow User Variables=True", url, dbUser, dbPass);
			}
		}

		/// <summary>
		/// Get user from DB by a user ID
		/// </summary>
		/// <returns>User object for that UID or null</returns>
		/// <param name="UID">User ID to retrieve</param>
		public static UserObject getUserByID(string UID) {
			UserObject retUser = null;

			List<MySqlParameter> pms = new List<MySqlParameter>{
				new MySqlParameter("@uid", UID)
			};

			string qry = "SELECT * FROM USER WHERE UserID LIKE @uid";

			KeyValuePair<bool, DataTable> qryRes = execQuery(qry, pms);

			if (qryRes.Key) {
				retUser = new UserObject();

				DataRow curUser = qryRes.Value.Rows[0];

				retUser.UID = (string)curUser["UserID"];
				retUser.EmailAddr = (string)curUser["Email"];
				retUser.Username = (string)curUser["Username"];
				retUser.Reputation = (float)curUser["Reputation"];
				retUser.DateCreated = (DateTime)curUser["DateCreated"];
				retUser.LastLogin = (DateTime)curUser["LastLogin"];
			}
			else {
				Console.WriteLine("Error getting user details");
			}

			return retUser;
		}

		public static UserObject getUserByUName(string uname) {
			UserObject retUser = null;

			int lastLogCol = 6; //Has got to be a better way to do this.

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				string qry = "SELECT * FROM USER WHERE Username LIKE @uname";

				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@uname", uname);

					try {
						conn.Open();

						MySqlDataReader reader = cmd.ExecuteReader();

						reader.Read();

						retUser = new UserObject(
							reader.GetString("UserID"),
							reader.GetString("Email"),
							reader.GetString("Username"),
							reader.GetFloat("Reputation"),
							reader.GetBoolean("Verified"),
							reader.GetDateTime("DateCreated"),
							reader.GetInt32("UserFlags")
						);

						if (!reader.IsDBNull(lastLogCol)) {
							retUser.LastLogin = reader.GetDateTime(lastLogCol);
						}
						else {
							retUser.LastLogin = null;
						}
					}
					catch (MySqlException ex) {
						Console.WriteLine("Get user by uname error: " + ex.Message);
					}
				}
			}

			return retUser;
		}

		/// <summary>
		/// Get Post from DB by post ID.
		/// </summary>
		/// <returns>Post object of that PID or null</returns>
		/// <param name="PID">Post ID to retrieve</param>
		public static Post getPostByID(int PID) {
			Post retPost = null;

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				string qry = "SELECT PostDetailView.* FROM PostDetailView WHERE PostID LIKE (@pid);";

				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@pid", PID);

					try {
						conn.Open();

						MySqlDataReader reader = cmd.ExecuteReader();

						reader.Read();

						retPost = readToPost(reader, 7, 10);

					}
					catch (MySqlException ex) {
						Console.WriteLine("Get Post error: " + ex.Message);
					}
				}

			}
			return retPost;
		}

		public static List<Post> getPostsSection(int fromRow, int num) {
			List<Post> retList = new List<Post>();

			using (MySqlConnection conn = new MySqlConnection(connStr)) {

				string qry = "SET @rownum=0; SELECT * FROM (SELECT (@rownum:=@rownum+1) AS rowNum, PostDetailView.* FROM PostDetailView) AS postRows WHERE rowNum >= " + fromRow + " LIMIT " + num + ";";

				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					try {
						conn.Open();

						MySqlDataReader reader = cmd.ExecuteReader();

						while (reader.Read()) {
							retList.Add(readToPost(reader));
						}

					}
					catch (MySqlException ex) {
						Console.WriteLine("Get Post section error: " + ex.Code + " " + ex.Message);
					}
				}

			}

			return retList;
		}

		/// <summary>
		/// Main post retrival function.
		/// Note: Set limit to 0 for infinite. This will probably come back to byte me.
		/// </summary>
		/// <param name="filter"></param>
		/// <param name="cmdParams"></param>
		/// <param name="fromRow"></param>
		/// <param name="limit"></param>
		/// <param name="orderBy"></param>
		/// <returns></returns>
		public static List<Post> getPostsBySearch(string filter, List<MySqlParameter> cmdParams, int fromRow = 0, int limit = 0, string orderBy = null) {
			List<Post> retList = new List<Post>();

			string orderStr = "";
			if (!String.IsNullOrWhiteSpace(orderBy)) {
				orderStr = " ORDER BY " + orderBy;
			}

			string limitStr = "";
			if (limit != 0) {
				limitStr = " LIMIT " + limit;
			}

			string qry = "SET @rownum=0; SELECT * FROM (SELECT (@rownum:=@rownum+1) AS rowNum, PostDetailView.* FROM PostDetailView) AS psv WHERE rowNum >= " + fromRow + " AND " + filter + orderStr + limitStr + ";";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {

				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					try {
						cmd.Parameters.AddRange(cmdParams.ToArray());

						foreach (MySqlParameter prm in cmdParams) {
							Console.WriteLine("Param: " + prm.Value);
						}

						Console.WriteLine("Src qry: " + cmd.CommandText);

						conn.Open();

						MySqlDataReader reader = cmd.ExecuteReader();

						while (reader.Read()) {
							retList.Add(readToPost(reader));
						}

					}
					catch (MySqlException ex) {
						Console.WriteLine("Get Post error: " + ex.Message);
					}
				}

			}

			return retList;
		}

		public static List<Marker> getAllMarkers() {
			List<Marker> retMarks = new List<Marker>();

			string qry = "SELECT * FROM PostMarkerView;";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					try {
						conn.Open();

						MySqlDataReader reader = cmd.ExecuteReader();

						while (reader.Read()) {
							retMarks.Add(readToMarker(reader));
						}
					}
					catch (MySqlException ex) {
						Console.WriteLine("Error get marker: " + ex.Message);
					}
				}
			}

			return retMarks;
		}

		/// <summary>
		/// Make a Post object from a MySqlDataReader row.
		/// Note: Does not change the reader position. This must be done by caller.
		/// </summary>
		/// <returns>The Post object</returns>
		/// <param name="reader">Reader instance</param>
		private static Post readToPost(MySqlDataReader reader, int uidIdx = 8, int unameIdx = 11) {

			Post rowPost = new Post(
				reader.GetInt32("PostID"),
				reader.GetString("Title"),
				reader.GetString("Description"),
				reader.GetInt32("Status"),
				reader.GetDateTime("Date"),
				reader.GetString("Path"),
				reader.GetInt32("PostFlags")
			);

			string tagsCSV = reader.GetString("Tags");

			rowPost.Tags = tagsCSV.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();

			if (reader.IsDBNull(uidIdx) == false && reader.IsDBNull(unameIdx) == false) {
				rowPost.UserID = reader.GetString(uidIdx);
			}
			else {
				rowPost.UserID = null;
			}

			//Fixed in PostDetailView with case. Should come up as Anon.
			rowPost.Username = reader.GetString(unameIdx);

			MySqlGeometry pos = reader.GetMySqlGeometry("Coordinate");

			rowPost.MarkerCoord = new Tuple<double?, double?>(pos.XCoordinate, pos.YCoordinate);

			return rowPost;
		}

		/// <summary>
		/// Makes a Marker object from a MySqlDataReader row. Should be used with PostMarkerView.
		/// Note: Does not change the reader position. This must be done by caller.
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		private static Marker readToMarker(MySqlDataReader reader) {
			Marker retMark = null;

			try {
				retMark = new Marker(
					reader.GetInt32("PostID"),
					reader.GetMySqlGeometry("Coordinate")
				);

				retMark.SetPostTitle(reader.GetString("Title"));
			}
			catch (MySqlException ex) {
				Console.WriteLine("Read to marker error: " + ex.Message);
			}

			return retMark;
		}

		public static int getPostsCount() {
			int pCnt = 0;

			string qry = "SELECT COUNT(*) FROM POST;";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					try {
						conn.Open();

						MySqlDataReader reader = cmd.ExecuteReader();

						reader.Read();

						pCnt = reader.GetInt32(0);
					}
					catch (MySqlException ex) {
						Console.WriteLine("Post count error: " + ex.Message);
					}
				}
			}

			return pCnt;
		}

		public static bool? getVote(string uid, int pid) {
			bool? voteState = null;

			string qry = "SELECT UpVote FROM VOTE WHERE UserID LIKE (@uid) AND PostID = @pid;";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@uid", uid);
					cmd.Parameters.AddWithValue("@pid", pid);

					try {
						conn.Open();

						MySqlDataReader reader = cmd.ExecuteReader();
						Console.WriteLine("Vote read");
						if (reader.HasRows) {
							reader.Read();
							Console.WriteLine("Vote has rows");
							voteState = reader.GetBoolean(0);
						}
					}
					catch (MySqlException ex) {
						Console.WriteLine("Get vote error error: " + ex.Message);
					}
				}
			}

			return voteState;
		}

		/// <summary>
		/// Get all votes on posts by a user.
		/// </summary>
		/// <returns>List of PID and isUpvote</returns>
		/// <param name="uid">UserID</param>
		public static List<KeyValuePair<int, bool>> getVotesByUser(string uid) {
			throw new NotImplementedException("getVotesByUser not made yet");
		}

		public static KeyValuePair<bool, int> InsertVote(string UID, int PID, bool isUpvote) {
			bool isSuccess = false;
			int returnCode = -1;

			string qry = "INSERT INTO VOTE (UserID, PostID, UpVote) VALUES (@uid, @pid, @vte) ON DUPLICATE KEY UPDATE UpVote=@vte;";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@uid", UID);
					cmd.Parameters.AddWithValue("@pid", PID);

					if (isUpvote) {
						cmd.Parameters.AddWithValue("@vte", 1);
					}
					else {
						cmd.Parameters.AddWithValue("@vte", 0);
					}

					try {
						conn.Open();

						int result = cmd.ExecuteNonQuery();

						Console.WriteLine("Insert vote success: " + result);

						isSuccess = true;
					}
					catch (MySqlException ex) {
						isSuccess = false;
						returnCode = ex.ErrorCode;

						Console.WriteLine("Vote SQL error: " + ex.Message);
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, returnCode);
		}

		/// <summary>
		/// Remove a vote. 
		/// </summary>
		/// <returns>true if successful, error code</returns>
		/// <param name="UID">User ID (Not username)</param>
		/// <param name="PID">Post ID</param>
		public static KeyValuePair<bool, int> RemoveVote(string UID, int PID) {
			bool isSuccess = false;
			int returnCode = -1;

			string qry = "DELETE FROM VOTE WHERE UserID LIKE(@uid) AND PostID=@pid;";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@uid", UID);
					cmd.Parameters.AddWithValue("@pid", PID);

					try {
						conn.Open();

						int result = cmd.ExecuteNonQuery();

						Console.WriteLine("Remove vote success: " + result);

						isSuccess = true;
					}
					catch (MySqlException ex) {
						isSuccess = false;
						returnCode = ex.ErrorCode;

						Console.WriteLine("Remove vote SQL error: " + ex.Message);
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, returnCode);
		}

		public static KeyValuePair<bool, int> getFlags(string uid, int pid) {
			bool isSuccess = false;
			int flagsVal = 0;

			string qry = "SELECT Flags FROM FLAGS WHERE UserID LIKE (@uid) AND PostID = @pid;";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					try {
						cmd.Parameters.AddWithValue("@uid", uid);
						cmd.Parameters.AddWithValue("@pid", pid);

						conn.Open();

						MySqlDataReader reader = cmd.ExecuteReader();

						if (reader.HasRows) {
							reader.Read();
							flagsVal = reader.GetInt32("Flags");
							Console.WriteLine("Get flags success: " + flagsVal);
						}

						isSuccess = true;
					}
					catch (MySqlException ex) {
						Console.WriteLine("SQL get flags error: " + ex.Message);
						isSuccess = false;
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, flagsVal);
		}

		public static KeyValuePair<bool, int> InsertFlag(string uid, int pid, int flag) {
			bool isSuccess = false;
			int returnCode = -1;

			string qry = "INSERT INTO FLAGS (UserID, PostID, Flags) VALUES (@uid, @pid, " + flag + ") ON DUPLICATE KEY UPDATE Flags = Flags | " + flag + ";";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@uid", uid);
					cmd.Parameters.AddWithValue("@pid", pid);

					try {
						conn.Open();

						int result = cmd.ExecuteNonQuery();

						Console.WriteLine("Insert flag success: " + result);

						isSuccess = true;
					}
					catch (MySqlException ex) {
						isSuccess = false;
						returnCode = ex.ErrorCode;

						Console.WriteLine("Flag insert SQL error: " + ex.Message);
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, returnCode);
		}

		public static KeyValuePair<bool, int> RemoveFlag(string uid, int pid, int flag) {
			bool isSuccess = false;
			int returnCode = -1;

			string qry = "UPDATE FLAGS SET Flags = Flags & ~@flg WHERE UserID LIKE (@uid) AND PostID = @pid;";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@uid", uid);
					cmd.Parameters.AddWithValue("@pid", pid);
					cmd.Parameters.AddWithValue("@flg", flag);

					try {
						conn.Open();

						int result = cmd.ExecuteNonQuery();

						Console.WriteLine("Remove flag success: " + result);

						isSuccess = true;
					}
					catch (MySqlException ex) {
						isSuccess = false;
						returnCode = ex.ErrorCode;

						Console.WriteLine("Flag remove SQL error: " + ex.Message);
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, returnCode);
		}

		public static KeyValuePair<bool, int> AddPostFlag(int pid, Post.Flags flag) {
			string qry = "UPDATE POST SET PostFlags = PostFlags | @flg WHERE PostID = @pid;";

			List<MySqlParameter> p = new List<MySqlParameter>{
				new MySqlParameter("@pid", pid),
				new MySqlParameter("@flg", (int)flag)
			};

			KeyValuePair<bool, int> res = execNonQuery(qry, p);

			return res;
		}

		public static KeyValuePair<bool, int> RemovePostFlag(int pid, Post.Flags flag) {
			string qry = "UPDATE POST SET PostFlags = PostFlags & ~@flg WHERE PostID = @pid;";

			List<MySqlParameter> p = new List<MySqlParameter>{
				new MySqlParameter("@pid", pid),
				new MySqlParameter("@flg", (int)flag)
			};

			KeyValuePair<bool, int> res = execNonQuery(qry, p);

			return res;
		}

		/// <summary>
		/// Add new User to database.
		/// </summary>
		/// <returns><c>true</c>, if successful insertion <c>false</c> if insert failed</returns>
		/// <param name="UID">UserID for primary key. This MUST be the same as the Firebase UID.</param>
		/// <param name="email">Email. Should be the same as is Firebase records. Stored in both places to ease api usage.</param>
		/// <param name="username">Username.</param>
		public static KeyValuePair<bool, int> InsertUser(string UID, string email, string username) {
			bool isSuccess = false;
			int errorCode = -1;

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				string qry = "INSERT INTO USER (UserID,Email,Username,DateCreated) VALUES (@uid,@email,@user,NOW());";

				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@uid", UID);
					cmd.Parameters.AddWithValue("@email", email);
					cmd.Parameters.AddWithValue("@user", username);

					try {
						conn.Open();

						int result = cmd.ExecuteNonQuery();

						if (result < 0) {
							Console.WriteLine("DB Insert User error: " + result);
							errorCode = 1105; //Code for unknown error
							isSuccess = false;
						}
						else {
							Console.WriteLine("Insert User success");
							isSuccess = true;
						}
					}
					catch (MySqlException ex) {
						isSuccess = false;
						errorCode = ex.Number;

						switch (ex.Number) {
							case 1062:
								Console.WriteLine("DB Duplicate entry error (1062): " + ex.Message);
								break;
							default:
								Console.WriteLine("DB Error " + ex.Number + ": " + ex.Message);
								break;
						}
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, errorCode);
		}

		/// <summary>
		/// Checks if the username is already in use.
		/// Note: seems okay static but not sure.
		/// </summary>
		/// <returns><c>true</c>, if the username is in use, <c>false</c> otherwise.</returns>
		/// <param name="uname">Uname.</param>
		public static bool CheckUsernameInUse(string uname) {
			bool inUse = true;

			string unameQry = String.Format("SELECT COUNT(*) FROM USER WHERE Username LIKE '{0}';", uname);

			using (MySqlConnection conn = new MySqlConnection(connStr)) {

				using (MySqlCommand cmd = new MySqlCommand(unameQry, conn)) {
					conn.Open();

					MySqlDataReader result = cmd.ExecuteReader();
					result.Read();
					if (result.GetInt32(0) == 0) {
						inUse = false;
					}
					else {
						inUse = true;
					}
				}
			}

			return inUse;
		}

		/// <summary>
		/// Checks if an email address is already assigned to a user.
		/// </summary>
		/// <returns><c>true</c>, if email is in use, <c>false</c> otherwise.</returns>
		/// <param name="email">Email.</param>
		public static bool CheckEmailInUse(string email) {
			bool inUse = true;

			string emailQry = String.Format("SELECT COUNT(*) FROM USER WHERE Email LIKE '{0}';", email);

			using (MySqlConnection conn = new MySqlConnection(connStr)) {

				using (MySqlCommand cmd = new MySqlCommand(emailQry, conn)) {
					conn.Open();

					MySqlDataReader result = cmd.ExecuteReader();
					result.Read();
					if (result.GetInt32(0) == 0) {
						inUse = false;
					}
					else {
						inUse = true;
					}
				}
			}

			return inUse;
		}

		public static KeyValuePair<bool, int> UpdateUserEmail(string UID, string newEmail) {
			bool isSuccess = false;
			int returnCode = -1;

			string qry = "UPDATE USER SET Email=@email WHERE UserID LIKE (@uid);";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@uid", UID);
					cmd.Parameters.AddWithValue("@email", newEmail);

					try {
						conn.Open();

						int result = cmd.ExecuteNonQuery();

						Console.WriteLine("Update email success: " + result);

						isSuccess = true;
					}
					catch (MySqlException ex) {
						isSuccess = false;
						returnCode = ex.ErrorCode;

						Console.WriteLine("Update email SQL error: " + ex.Message);
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, returnCode);
		}

		public static bool PopulateUserModel(UserModel uMod) {
			if (uMod.UID == null) {
				return false;
			}

			bool status = false;
			List<MySqlParameter> pms = new List<MySqlParameter>{
				new MySqlParameter("@uid", uMod.UID)
			};

			string qry = "SELECT * FROM USER WHERE UserID LIKE @uid;";


			KeyValuePair<bool, DataTable> res = execQuery(qry, pms);

			if (res.Key) {

				DataRow curUser = res.Value.Rows[0];

				uMod.UID = (string)curUser["UserID"];
				uMod.EmailAddr = (string)curUser["Email"];
				uMod.Username = (string)curUser["Username"];
				uMod.Reputation = (float)curUser["Reputation"];
				uMod.DateCreated = (DateTime)curUser["DateCreated"];
				uMod.UserFlags=(int)curUser["UserFlags"];

				if (curUser["LastLogin"] != DBNull.Value) {
					uMod.LastLogin = (DateTime)curUser["LastLogin"];
				}

				status = true;
			}
			else {
				Console.WriteLine("Pop umod error");
				status = false;
			}

			return status;
		}

		/// <summary>
		/// Sets the user last login to now.
		/// </summary>
		/// <param name="uid">User ID for user to update.</param>
		/// <returns>True if success and error code</returns>
		public static KeyValuePair<bool, int> UpdateUserLastLog(string uid) {
			bool isSuccess = false;
			int returnCode = -1;

			string qry = "UPDATE USER SET LastLogin=NOW() WHERE UserID LIKE (@uid);";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {
				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@uid", uid);

					try {
						conn.Open();

						int result = cmd.ExecuteNonQuery();

						Console.WriteLine("Update lastlog success: " + result);

						isSuccess = true;
					}
					catch (MySqlException ex) {
						isSuccess = false;
						returnCode = ex.ErrorCode;

						Console.WriteLine("Update lastlog SQL error: " + ex.Message);
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, returnCode);
		}

		//Next up
		public static KeyValuePair<bool, int> InsertPost(string title, string desc, string tags, string UID, int markerID, int imgID) {
			bool isSuccess = false;
			int returnCode = -1;

			string qry = "INSERT INTO POST (Title, Description, Tags, Status, UserID, MarkID, ImgID) VALUES (@title, @desc, @tags, @status, @uid, @mid, @iid); SELECT LAST_INSERT_ID();";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {

				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@title", title);
					cmd.Parameters.AddWithValue("@desc", desc);
					cmd.Parameters.AddWithValue("@tags", tags);
					cmd.Parameters.AddWithValue("@status", 0); //Might preload uploader updoot
															   //UID
					cmd.Parameters.AddWithValue("@mid", markerID);
					cmd.Parameters.AddWithValue("@iid", imgID);

					if (string.IsNullOrEmpty(UID)) {
						cmd.Parameters.AddWithValue("@uid", DBNull.Value);
					}
					else {
						cmd.Parameters.AddWithValue("@uid", UID);
					}

					try {
						conn.Open();

						MySqlDataReader result = cmd.ExecuteReader();

						result.Read();

						//Return the new PK int.
						returnCode = result.GetInt32(0);

						isSuccess = true;

						Console.WriteLine("Post insert success");
					}
					catch (MySqlException ex) {
						isSuccess = false;
						returnCode = ex.ErrorCode;

						Console.WriteLine("Post SQL error: " + ex.Message);
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, returnCode);
		}

		public static KeyValuePair<bool, int> InsertMarker(float lng, float lat) {
			bool isSuccess;
			int returnCode = -1;

			string qry = "INSERT INTO MARKER (Coordinate) VALUES (ST_GeomFromText(@coords)); SELECT LAST_INSERT_ID();";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {

				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@coords", String.Format("POINT({0} {1})", lng, lat));

					try {
						conn.Open();

						MySqlDataReader result = cmd.ExecuteReader();

						result.Read();

						//Return the new PK int.
						returnCode = result.GetInt32(0);

						isSuccess = true;

						Console.WriteLine("Marker insert success");
					}
					catch (MySqlException ex) {
						isSuccess = false;
						returnCode = ex.ErrorCode;

						Console.WriteLine("Marker SQL error: " + ex.Message);
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, returnCode);
		}

		public static KeyValuePair<bool, int> InsertImage(string path) {
			bool isSuccess;
			int returnCode = -1;

			string qry = "INSERT INTO IMAGE (Path) VALUES (@path); SELECT LAST_INSERT_ID();";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {

				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@path", path);

					try {
						conn.Open();

						MySqlDataReader result = cmd.ExecuteReader();

						result.Read();

						//Return the new PK int.
						returnCode = result.GetInt32(0);

						isSuccess = true;

						Console.WriteLine("Insert image success");
					}
					catch (MySqlException ex) {
						isSuccess = false;
						returnCode = ex.ErrorCode;

						Console.WriteLine("Image SQL error: " + ex.Message);
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, returnCode);
		}

		public static KeyValuePair<bool, int> DeletePost(int pid) {
			bool isSuccess;
			int returnCode = -1;

			string qry = "DELETE FROM POST WHERE PostID = @pid";

			using (MySqlConnection conn = new MySqlConnection(connStr)) {

				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddWithValue("@pid", pid);

					try {
						conn.Open();

						int result = cmd.ExecuteNonQuery();

						returnCode = result;

						if (result > 0) {
							isSuccess = true;

							Console.WriteLine("Delete post success");
						}
						else {
							Console.WriteLine("Delete post no rows affected");
							isSuccess = false;
						}
					}
					catch (MySqlException ex) {
						isSuccess = false;
						returnCode = ex.ErrorCode;

						Console.WriteLine("Delete post SQL error: " + ex.Message);
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, returnCode);
		}

		private static KeyValuePair<bool, int> execNonQuery(string qry, List<MySqlParameter> prms) {
			bool isSuccess;
			int returnCode = -1;

			using (MySqlConnection conn = new MySqlConnection(connStr)) {

				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddRange(prms.ToArray());

					try {
						conn.Open();

						int result = cmd.ExecuteNonQuery();

						//Return the new PK int.
						returnCode = result;

						if (result == 0) {
							isSuccess = false;
							Console.WriteLine("Nonquery error: No rows affected");
						}
						else {
							isSuccess = true;
							Console.WriteLine("Nonqry success");
						}
					}
					catch (MySqlException ex) {
						isSuccess = false;
						returnCode = ex.ErrorCode;

						Console.WriteLine("Nonqry error: " + ex.Message);
					}
				}
			}

			return new KeyValuePair<bool, int>(isSuccess, returnCode);
		}

		/// <summary>
		/// Execute query string and return DataTable.
		/// </summary>
		/// <returns>The query.</returns>
		/// <param name="qry">Qry.</param>
		private static KeyValuePair<bool, DataTable> execQuery(string qry, List<MySqlParameter> prms) {
			DataTable retTab = new DataTable();
			bool isSuccess = false;

			using (MySqlConnection conn = new MySqlConnection(connStr)) {

				using (MySqlCommand cmd = new MySqlCommand(qry, conn)) {
					cmd.Parameters.AddRange(prms.ToArray());

					try {
						conn.Open();

						MySqlDataReader result = cmd.ExecuteReader();

						retTab.Load(result);
						isSuccess = true;
					}
					catch (MySqlException ex) {
						Console.WriteLine("Qry error: " + ex.Message);
						isSuccess = false;
					}
				}
			}

			return new KeyValuePair<bool, DataTable>(isSuccess, retTab);
		}
	}
}
