//General map to use
var coordPrec=4;

var targPos=ol.proj.fromLonLat([0,0]);

var mapView=new ol.View({
	center: targPos,
	zoom: 4
});

var mapOpts={
	target: "",
	layers: [
		new ol.layer.Tile({
			source: new ol.source.OSM()
		})
	],
	view: mapView
};

var mkrStyle=new ol.style.Style({
	image: new ol.style.Circle({
		fill: new ol.style.Fill({
			color: 'red'
		}),
		stroke: new ol.style.Stroke({
			color: 'black',
			width: 1
		}),
		radius: 7
	}),
	text: new ol.style.Text({
		text: "Nothing..."
	})
});

var vLayer=new ol.layer.Vector({
	source: new ol.source.Vector()
});

var map;

function initMap(){
	console.log("Init map");
	map=new ol.Map(mapOpts);
	map.addLayer(vLayer);
}

function initMap(ele){
	mapOpts.target=ele;
	map=new ol.Map(mapOpts);
	map.addLayer(vLayer);
}

function initMap(ele, lon, lat){
	mapOpts.target=ele;
	mapOpts.view.setCenter(ol.proj.fromLonLat([lon,lat]));
	map=new ol.Map(mapOpts);
	map.addLayer(vLayer);
}

function setCenter(lon, lat){
	mapView.setCenter(ol.proj.fromLonLat([lon,lat]));
}

function addMarker(nme, lon, lat){
	var mkr=new ol.Feature({
		geometry: new ol.geom.Point(
			ol.proj.fromLonLat([lon,lat])
		),
		name: nme
	});

	mkr.setStyle(mkrStyle.clone());
	mkr.getStyle().getText().setText(mkr.get("name"));

	vLayer.getSource().addFeature(mkr);
}