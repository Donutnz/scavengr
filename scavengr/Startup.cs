﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using scavengr.Models;
using scavengr.Controllers;
using Microsoft.AspNetCore.Http;
using scavengr.Middleware;

namespace scavengr {
	public class Startup {

		public Startup(IConfiguration configuration) {
			Console.WriteLine("Start up called");
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services) {
			services.AddMvc(options => {
				//options.Filters.Add(new SessionFilter());
			});

			services.AddAntiforgery();
			services.AddDistributedMemoryCache();
			services.AddSession();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
			if(env.IsDevelopment()) {
				app.UseDeveloperExceptionPage();
				//app.UseExceptionHandler("/Home/Error"); //For testing the error page.
			}
			else {
				app.UseExceptionHandler("/Home/Error");
			}

			app.UseStaticFiles();

			app.UseSession();

			app.Use((context, next) => {
				Console.WriteLine("MW Sess: " + context.Session.Id);
				return next();
			});

			app.UseUserSession();

			app.UseMvc(routes => {
				//PD Route for when post ID is specified.
				routes.MapRoute(name: "PostDetail",
				                template: "PostDetail/{pid:int}",
								defaults: new { controller = "PostDetail", action = "Index" });

				//PD route for everything else (esp POST).
				routes.MapRoute(name: "OtherPD",
								template: "PostDetail/{action}");

				//Need to deal with uname or something else.
				routes.MapRoute("UserDetail", "UserDetail/{uname}",
								defaults: new { controller = "UserDetail", action = "Index" });

				//Custom route because page is too basic to need a dedicated controller.
				routes.MapRoute("AboutPage", "About",
								defaults: new { controller = "Home", action = "About" });

				//Same as About.
				routes.MapRoute("ContactPage", "Contact",
								defaults: new { controller = "Home", action = "Contact" });
				
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");
			});
		}
	}
}
