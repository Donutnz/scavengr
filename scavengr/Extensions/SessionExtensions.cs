using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using scavengr.Models;

namespace scavengr.Extensions {
	public static class SessionExtensions {
		public static void SetUserModel(this ISession session, string key, UserModel value) {
			session.SetString(key, JsonConvert.SerializeObject(value));
		}

		public static UserModel GetUserModel(this ISession session, string key) {
			string mod = session.GetString(key);

			if (mod != null) {
				return JsonConvert.DeserializeObject<UserModel>(mod);
			}
			else {
				Console.WriteLine("ret null umod");
				return null;
			}
		}

		/// <summary>
		/// Set a bool value in session.
		/// </summary>
		/// <param name="session"></param>
		/// <param name="key"></param>
		/// <param name="val"></param>
		public static void SetBool(this ISession session, string key, bool val) {
			int intVal;
			if (val) {
				intVal = 1;
			}
			else {
				intVal = 0;
			}

			session.SetInt32(key, intVal);
		}

		/// <summary>
		/// Get a stored bool value. 
		/// Note: can return false.
		/// </summary>
		/// <param name="sess"></param>
		/// <param name="key"></param>
		/// <returns>True, false, or null</returns>
		public static bool? GetBool(this ISession sess, string key) {
			int? val = sess.GetInt32(key);

			if (val == 1) {
				return true;
			}
			else if (val == 0) {
				return false;
			}
			else {
				return null;
			}
		}

		/// <summary>
		/// Similar to GetBool but returns false if no value found.
		/// Caution: Will return false on null. Take this into consideration.
		/// </summary>
		/// <param name="sess"></param>
		/// <param name="key"></param>
		/// <returns>True if value is true. False if value is false or null.</returns>
		public static bool GetBoolNN(this ISession sess, string key) {
			int? val = sess.GetInt32(key);

			if (val == 1) {
				return true;
			}
			else {
				return false;
			}
		}
	}
}