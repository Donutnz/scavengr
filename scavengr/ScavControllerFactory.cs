﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using System.Web;
using scavengr.Controllers;
using Microsoft.AspNetCore.Mvc.Internal;
using System.Collections.Generic;
using System.Linq;
using scavengr.Models;

namespace scavengr {
	public class ScavControllerFactory:IControllerFactory {
		private IControllerActivator _contAct;
		private IControllerPropertyActivator[] _propActs;

		private UserModel _uMod;
		public UserModel userMod {
			get {
				if(_uMod == null) {
					_uMod = new UserModel();
				}

				return _uMod;
			}
		}

		public ScavControllerFactory(IControllerActivator ICA, IEnumerable<IControllerPropertyActivator> propActs) {
			_contAct = ICA;
			_propActs = propActs.ToArray();
			Console.WriteLine("ContFact constructed");
		}

		public object CreateController(ControllerContext ctx) {
			Console.WriteLine("Custom cont fact create");

			//var controller = new HomeController();
			//var controller =_contAct.Create(ctx);
			object controller;
			if(ctx.ActionDescriptor.ControllerTypeInfo.DeclaredConstructors.Count() > 1) {
				object[] args = { userMod };
				controller = Activator.CreateInstance(ctx.ActionDescriptor.ControllerTypeInfo.AsType(), args);
			}
			else {
				controller = Activator.CreateInstance(ctx.ActionDescriptor.ControllerTypeInfo.AsType());
			}

			foreach(var PA in _propActs) {
				Console.WriteLine("PA: " + PA.ToString());
				PA.Activate(ctx, controller);
			}

			return controller;
		}

		public void ReleaseController(ControllerContext ctx, object controller) {
			Console.WriteLine("Custom cont fact release");

			/*
			IDisposable disp = controller as IDisposable;
			if(disp != null){
				disp.Dispose();
			}
			*/
			_contAct.Release(ctx, controller);
		}
	}
}
