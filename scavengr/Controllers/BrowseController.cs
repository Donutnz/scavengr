﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using MySql.Data.MySqlClient;
using scavengr.DataObjects;
using scavengr.Managers;
using scavengr.Models;

namespace scavengr {
	public class BrowseController : Controller {
		private BrowseModel _brwMod;
		public BrowseModel model{
			get{
				if(_brwMod == null){
					_brwMod=new BrowseModel();
				}

				return _brwMod;
			}
		}

		// GET: /<controller>/
		public IActionResult Index() {
			//Reversed so that the earlier instances of a key are given preference
			foreach(KeyValuePair<string, StringValues> qry in HttpContext.Request.Query.Reverse()){
				Console.WriteLine("Key: " + qry.Key);
				Console.WriteLine("Val: " + qry.Value);

				ViewData[qry.Key] = qry.Value;
			}

			//Posts per page
			int pp;
			if(int.TryParse(HttpContext.Request.Query["perpage"], out pp)) {
				model.perPage = pp;
				HttpContext.Session.SetInt32("perpage", model.perPage);
			}
			else {
				model.perPage = HttpContext.Session.GetInt32("perpage") ?? model.perPage;
			}

			//Current page
			int cp;
			if (int.TryParse(HttpContext.Request.Query["page"], out cp)) {
				model.curPage = cp;
			}

			ViewData["PageCount"] = model.PageCount;
			ViewData["NextPage"] = model.NextPage;
			ViewData["PrevPage"] = model.PrevPage;

			//Sort
			//Filter
			ViewBag.Posts=model.GetPosts();

			ViewData["mkrJson"] = model.GetMarkers();

			return View();
		}		
	}
}
