﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;
using System.Threading;
using scavengr.Models;
using System.Text;
using Newtonsoft.Json.Linq;
using scavengr.Managers;
using scavengr.Extensions;

namespace scavengr.Controllers {
	/// <summary>
	/// Handler for logging in and signing up.
	/// </summary>
	public class LoginController : Controller {
		public IActionResult Index() {
			Console.WriteLine("Sess: " + HttpContext.Session.Id);

			if((bool)HttpContext.Items["loggedIn"]) {
				UserModel uMod = (UserModel)HttpContext.Items["CurrentUser"];

				ViewData["Email"] = uMod.EmailAddr;
				ViewData["UID"] = uMod.UID;
			}

			return View();
		}

		[HttpPost]
		public string SignUp(string email, string user, string pass) {
			Console.WriteLine("Email: " + email);
			Console.WriteLine("User: " + user);
			Console.WriteLine("Pass: " + pass);

			UserModel uMod = new UserModel();
			KeyValuePair<int, string> res = uMod.SignUp(email, user, pass);

			JObject retObj = new JObject {
				{ "ID", 0 },
				{ "Status", "null" },
				{ "Error", "null" }
			};

			if(res.Key == 0) {
				HttpContext.Session.Set("UID", Encoding.UTF8.GetBytes(uMod.UID));
				HttpContext.Session.SetUserModel(HttpContext.Session.Id, uMod);
				HttpContext.Session.SetBool("isLoggedIn", true);

				retObj["ID"] = 0;
				retObj["Status"] = "success";

				ViewBag.CurrentUser = uMod;
			}
			else {
				retObj["ID"] = 1;
				retObj["Status"] = "error";

				if(res.Key == 1) {
					retObj["Error"] = res.Value;
				}
				else if(res.Key == 2) {
					switch(Convert.ToInt32(res.Value)) {
						case 1062:
							retObj["Error"] = "Duplicate entry error";
							break;
						case 1105:
							retObj["Error"] = "Unknown DB error";
							break;
						default:
							retObj["Error"] = "Unknown error: " + res.Value;
							break;
					}
				}

			}

			return retObj.ToString();
		}

		[HttpPost]
		public string LogIn(string email, string pass) {
			Console.WriteLine("Sess POST: " + HttpContext.Session.Id);
			Console.WriteLine("Email: " + email);
			Console.WriteLine("Pass: " + pass);

			UserModel uMod = new UserModel();
			KeyValuePair<bool, string> res = uMod.SignIn(email, pass);

			JObject retObj = new JObject {
				{ "ID", 0 },
				{ "Status", "null" },
				{ "Error", "null" }
			};

			if(res.Key) {
				HttpContext.Session.Set("UID", Encoding.UTF8.GetBytes(uMod.UID));
				HttpContext.Session.SetUserModel(HttpContext.Session.Id, uMod);
				HttpContext.Session.SetBool("isLoggedIn", true);

				retObj["ID"] = 0;
				retObj["Status"] = "success";

				ViewBag.CurrentUser = uMod;
			}
			else {
				retObj["ID"] = 1;
				retObj["Status"] = "error";
				retObj["Error"] = res.Value;
			}

			return retObj.ToString();
		}

		/// <summary>
		/// Endpoint to check if an email addr is already in use.
		/// Note: Could this be a security risk? Not sure.
		/// </summary>
		/// <returns><c>true</c>, if email is in use, <c>false</c> otherwise.</returns>
		/// <param name="email">Email.</param>
		[HttpGet]
		public bool CheckEmail(string email) {
			bool inUse = DatabaseManager.CheckEmailInUse(email);
			return inUse;
		}

		/// <summary>
		/// Endpoint for checking if username is already in use.
		/// Note: Security risk?
		/// </summary>
		/// <returns><c>true</c>, if username is in use, <c>false</c> otherwise.</returns>
		/// <param name="username">Username.</param>
		[HttpGet]
		public bool CheckUsername(string username) {
			bool inUse = DatabaseManager.CheckUsernameInUse(username);
			return inUse;
		}
	}
}
