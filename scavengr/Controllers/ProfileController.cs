﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using scavengr.Extensions;
using scavengr.Managers;
using scavengr.Models;

namespace scavengr.Controllers {
	public class deleteObj{
		public int pid;
	}

	/// <summary>
	/// Handles settings and actions for the current user. Passwd changes, account delete, etc.
	/// </summary>
	public class ProfileController : Controller {
		private UserModel uMod {
			get {
				return (UserModel)HttpContext.Items["CurrentUser"];
			}
		}

		public IActionResult Index() {
			//Check if the user is logged in
			if(!(bool)HttpContext.Items["loggedIn"]) {
				return Redirect("/login");
			}

			if(uMod == null){
				Console.WriteLine("Umod null");
			}

			ViewBag.UserData = uMod;

			ViewBag.Posts=uMod.getPosts();

			return View();
		}
		
		public string Logout(){
			if(HttpContext.Session.GetBoolNN("isLoggedIn")){
				Console.WriteLine("Log out: "+((UserModel)HttpContext.Items["CurrentUser"]).Username);
			}

			HttpContext.Session.Clear();
			return "success";
		}

		public string changePassword(string email, string oldPasswd, string newPasswd){
			KeyValuePair<bool, string> res = uMod.ChangePassword(email, oldPasswd, newPasswd);

			JObject respObj = new JObject();

			Console.WriteLine("change pass: " + res.Value);

			if(res.Key) {
				respObj.Add("success", true);
				respObj.Add("message", "success");
			}
			else {
				respObj.Add("success", false);
				respObj.Add("message", res.Value);
			}

			return respObj.ToString();
		}

		public string changeEmail(string curEmail, string passwd, string newEmail){
			KeyValuePair<bool, string> res = uMod.ChangeEmail(curEmail, passwd, newEmail);

			JObject respObj = new JObject();

			Console.WriteLine("change email: " + res.Value);

			if(res.Key){
				respObj.Add("success", true);
				respObj.Add("message", "success");
			}
			else{
				respObj.Add("success", false);
				respObj.Add("message", res.Value);
			}

			return respObj.ToString();
		}

		public void deleteAccount(){
			throw new Exception("Not yet implemented");	
		}

		[HttpPost]
		public string deletePost([FromBody] deleteObj post){
			Console.WriteLine("Delete post: "+post.pid);

			JObject resp=new JObject();

			KeyValuePair<bool, int> res=DatabaseManager.DeletePost(post.pid);

			if(res.Key){
				resp.Add("success", true);
			}
			else{
				resp.Add("success", false);
			}

			return resp.ToString();
		}
	}
}
