﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using MySql.Data.MySqlClient;
using scavengr.DataObjects;
using scavengr.Managers;
using scavengr.Models;

namespace scavengr.Controllers {
	/// <summary>
	/// Handles the advanced search stuff.
	/// </summary>
	public class SearchController : Controller {
		private SearchModel _schMod;
		public SearchModel model {
			get {
				if (_schMod == null) {
					_schMod = new SearchModel();
				}

				return _schMod;
			}
		}
		//Having 'OR' as well would be cool. With | or something.
		//And more complex chars. Like * and stuff.

		// GET: /<controller>/
		public IActionResult Index() {
			
			foreach (KeyValuePair<string, StringValues> kv in HttpContext.Request.Query.Reverse()) {
				Console.WriteLine("Key: " + kv.Key + " Val: " + kv.Value);
				ViewData[kv.Key] = kv.Value;
			}

			//Posts per page
			int pp;
			if (int.TryParse(HttpContext.Request.Query["perpage"], out pp)) {
				model.perPage = pp;
				HttpContext.Session.SetInt32("perpage", model.perPage);
			}
			else {
				model.perPage = HttpContext.Session.GetInt32("perpage") ?? model.perPage;
			}

			//Maybe save cur page in session as well?

			//Current page
			int cp;
			if (int.TryParse(HttpContext.Request.Query["page"], out cp)) {
				model.curPage = cp;
			}

			ViewData["PageCount"] = model.PageCount;
			ViewData["NextPage"] = model.NextPage;
			ViewData["PrevPage"] = model.PrevPage;

			model.SetSort(HttpContext.Request.Query["sort"], HttpContext.Request.Query["order"]);

			if (!String.IsNullOrEmpty(HttpContext.Request.Query["qry"])) {
				ViewBag.Posts = model.GetPosts(HttpContext.Request.Query["qry"]);
			}
			else {
				ViewBag.Posts = model.GetPosts();
			}

			return View();
		}
	}
}
