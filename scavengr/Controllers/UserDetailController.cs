﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using scavengr.DataObjects;
using scavengr.Managers;

namespace scavengr.Controllers {
	public class UserDetailController : Controller {
		// GET: /<controller>/
		public IActionResult Index(string uname) {
			if(String.IsNullOrEmpty(uname)){
				Console.WriteLine("No user specified");
				throw new Exception("No username specified");
			}

			UserObject usr = DatabaseManager.getUserByUName(uname);

			if(usr == null){
				Console.WriteLine("User not found");
				throw new Exception("No user found");
			}

			ViewBag.User = usr;

			return View();
		}
	}
}
