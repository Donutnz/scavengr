﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using scavengr.Models;

namespace scavengr.Controllers {
	public class HomeController : Controller {

		public IActionResult Index() {
			Console.WriteLine("Showing index");
			return View();
		}

		public IActionResult About() {
			return View();
		}

		public IActionResult Contact() {
			return View();
		}

		public IActionResult Error() {
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
