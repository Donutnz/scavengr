﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using scavengr.DataObjects;
using scavengr.Models;
using scavengr.Managers;
using scavengr.Extensions;

namespace scavengr.Controllers {

	/// <summary>
	/// Only for receiving complex JSON object from vote.
	/// </summary>
	public class VoteObj {
		public int vid;
		public int pid;
	}

	public class FlagObj{
		public int flagID;
		public int pid;
	}

	public class PostDetailController : Controller {
		// GET: /<controller>/
		public IActionResult Index(int? pid) {
			if(pid == null) {
				Console.WriteLine("No PostID specified");
				throw new Exception("No PostID specified");
			}

			int nnPid = pid ?? default(int); //Convert to non-nullable

			Post pst = DatabaseManager.getPostByID(nnPid);

			if(pst != null) {
				ViewBag.Post = pst;
			}
			else {
				Console.WriteLine("Post null");
				throw new Exception("No post with that ID found");
			}

			if((bool)HttpContext.Items["loggedIn"]) {
				UserModel uMod = (UserModel)HttpContext.Items["CurrentUser"];
				
				bool? vote = DatabaseManager.getVote(uMod.UID, nnPid);
				ViewData["userVote"] = vote;

				KeyValuePair<bool,int> flags=DatabaseManager.getFlags(uMod.UID, nnPid);
				if(flags.Key){
					ViewData["flags"]=flags.Value;
				}
				else{
					//Need better handling for this.
					ViewData["flags"]=0;
				}
			}
			else{
				//Need better handling for this.
				ViewData["flags"]=0;
			}

			return View();
		}

		public string test() {
			return "Test page";
		}

		/// <summary>
		/// End point for status voteing.
		/// Could do with a tidy up.
		/// </summary>
		/// <returns>Json object with if logged in or not.</returns>
		/// <param name="vObj">Complex json object as post body.</param>
		[HttpPost]
		public string VotePost([FromBody] VoteObj vObj) {
			int statusVote = vObj.vid;

			Console.WriteLine("StatusVote: " + statusVote);

			JObject respObj = new JObject();

			if((bool)HttpContext.Items["loggedIn"]) {
				Console.WriteLine("Is logged in");
				respObj.Add("loggedIn", true);

				UserModel curUser = ((UserModel)HttpContext.Items["CurrentUser"]);

				if(statusVote == 0) {
					Console.WriteLine("No vote");
					DatabaseManager.RemoveVote(curUser.UID, vObj.pid);
				}
				else if(statusVote == 1) {
					Console.WriteLine("Up vote");
					DatabaseManager.InsertVote(curUser.UID, vObj.pid, true);
				}
				else if(statusVote == -1) {
					Console.WriteLine("Down vote");
					DatabaseManager.InsertVote(curUser.UID, vObj.pid, false);
				}
				else {
					Console.WriteLine("Unknown vote: " + statusVote);
				}
			}
			else {
				Console.WriteLine("Not logged in");
				respObj.Add("loggedIn", false);
			}

			return respObj.ToString();
		}

		/// <summary>
		/// Sets a user/post flag
		/// </summary>
		/// <param name="flag"></param>
		/// <returns></returns>
		[HttpPost]
		public string AddFlag([FromBody] FlagObj flag) {
			JObject respObj = new JObject();

			if(HttpContext.Session.GetBoolNN("isLoggedIn")){
				respObj.Add("loggedIn", true);

				UserModel curUser=HttpContext.Session.GetUserModel(HttpContext.Session.Id);

				//1 = removed flag
				KeyValuePair<bool, int> res=DatabaseManager.InsertFlag(curUser.UID, flag.flagID, 1);
			}
			else{
				Console.WriteLine("Not logged in");
				respObj.Add("loggedIn", false);
			}

			return respObj.ToString();
		}

		/// <summary>
		/// Sets a flag on the post itself.
		/// </summary>
		/// <param name="flag"></param>
		/// <returns></returns>
		[HttpPost]
		public string ConfFlag([FromBody] FlagObj flag){
			JObject respObj=new JObject();
			respObj.Add("success", false);
			respObj.Add("error", "nothing...");
			respObj.Add("loggedIn", false);

			if(HttpContext.Session.GetBoolNN("isLoggedIn")){
				respObj["loggedIn"]=true;

				UserModel curUser=HttpContext.Session.GetUserModel(HttpContext.Session.Id);

				if((curUser.UserFlags & (int)UserObject.Flags.EDITOR) > 0){
					KeyValuePair<bool, int> res=DatabaseManager.AddPostFlag(flag.pid, (Post.Flags)flag.flagID);

					if(res.Key){
						Console.WriteLine("Set flag success");
						respObj["success"]=true;
					}
					else{
						respObj["success"]=false;
						respObj["error"]= "sql error";
					}
				}
				else{
					Console.WriteLine("Set flag bad permissions");
					respObj["success"]=false;
					respObj["error"]="bad permissions";
				}
			}
			else{
				Console.WriteLine("Not logged in");
				respObj["loggedIn"]= false;
				respObj["success"]= false;
				respObj["error"]= "not logged in";
			}

			return respObj.ToString();
		}
	}
}
