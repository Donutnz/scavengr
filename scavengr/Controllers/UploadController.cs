﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using scavengr.Managers;
using scavengr.Models;

namespace scavengr.Controllers {
	/// <summary>
	/// Handle new post upload.
	/// </summary>
	public class UploadController : Controller {
		// GET: /<controller>/
		public IActionResult Index() {
			return View();
		}
		//Need to add logged in checking and prompt
		//174,36
		[HttpPost]
		public string Index(string title, string desc, string tags, string image, float lng, float lat){
			Console.WriteLine("Title: " + title);
			Console.WriteLine("Desc: " + desc);
			Console.WriteLine("Tags: " + tags);
			Console.WriteLine("Image: " + image);
			Console.WriteLine("Lat: " + lat);
			Console.WriteLine("Lng: " + lng);

			JObject retObj = new JObject {
				{"ID",0},
				{"Success", false},
				{"PostID",""},
				{"Error",""},
				{"MarkerSuccess", false},
				{"MarkerVal",-1},
				{"ImageSuccess", false},
				{"ImageVal",-1},
				{"PostSuccess", false},
				{"PostVal", -1}
			};

			string csvTags = tags.Replace(", ", ","); //CSV because sql doesn't really support arrays.
			csvTags=csvTags.Replace(' ', ',');
			csvTags = "," + csvTags + ","; //Makes searching way easier.

			KeyValuePair<bool, int> mkrRes;
			KeyValuePair<bool, int> imgRes;
			KeyValuePair<bool, int> postRes;

			if(!String.IsNullOrEmpty(image)){
				mkrRes=DatabaseManager.InsertMarker(lng, lat);
				retObj["MarkerSuccess"] = mkrRes.Key;
				retObj["MarkerVal"] = mkrRes.Value;

				if(mkrRes.Key){
					imgRes=DatabaseManager.InsertImage(image);
					retObj["ImageSuccess"] = imgRes.Key;
					retObj["ImageVal"] = imgRes.Value;

					if(imgRes.Key){
						string UID = null;

						if(HttpContext.Items["CurrentUser"] != null){
							UID = ((UserModel)HttpContext.Items["CurrentUser"]).UID;
						}

						postRes=DatabaseManager.InsertPost(title, desc, csvTags, UID, mkrRes.Value, imgRes.Value);
						retObj["PostSuccess"] = postRes.Key;
						retObj["PostVal"] = postRes.Value;

						if(postRes.Key){
							retObj["PostID"] = postRes.Value;
							retObj["Success"] = true;
						}
					}
				}
			}
			else{
				Console.WriteLine("Upload error: No image");
				retObj["Error"] = "No image found";
			}

			return retObj.ToString();
		}
	}
}
