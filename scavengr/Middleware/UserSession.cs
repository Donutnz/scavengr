﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using scavengr.Extensions;
using scavengr.Models;

namespace scavengr.Middleware {
	/// <summary>
	/// Used to inject session specific complex user data into request.
	/// Replaces SessionFilter.
	/// </summary>
	[Obsolete("This is legacy. Use HttpContext.Session instead where possible.")]
	public class UserSession {
		private readonly RequestDelegate _next;

		public UserSession(RequestDelegate next) {
			_next = next;
		}

		public Task Invoke(HttpContext ctx) {
			Console.WriteLine("Invoke");

			UserModel uMod=ctx.Session.GetUserModel(ctx.Session.Id);

			if(uMod != null){
				ctx.Items["CurrentUser"]=uMod;
				ctx.Items["loggedIn"]=true;
			}
			else{
				ctx.Items["loggedIn"]=false;
			}

			return _next(ctx);
		}
	}

	// Extension method used to add the middleware to the HTTP request pipeline.
	public static class UserSessionExtensions {
		public static IApplicationBuilder UseUserSession(this IApplicationBuilder builder) {
			return builder.UseMiddleware<UserSession>();
		}
	}
}
