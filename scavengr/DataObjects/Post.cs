﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using scavengr.Managers;

namespace scavengr.DataObjects {
	public class Post {
		public int PID;
		public string Title;
		public string Description;
		public List<string> Tags;
		public int Status;
		public DateTime Date;
		public int PostFlags;
		public string UserID; //Can be null.
		public string Username;
		//public int ImageID;
		public string ImagePath;
		//public int MarkerID;
		public Tuple<double?, double?> MarkerCoord;

		/// <summary>
		/// Flags for post. Used for bitwise.
		/// Used for post and user-post tables.
		/// </summary>
		public enum Flags {
			NONE, MOVED, USER_BANNED, INAPPROPRIATE 
		}

		public Post() {
			Console.WriteLine("Init Post()");
		}

		public Post(int pid, string title, string desc, int status, DateTime dte, string img, int pstFlgs) {
			PID = pid;
			Title = title;
			Description = desc;
			Status = status;
			Date = dte;
			ImagePath = img;
			PostFlags = pstFlgs;

			Console.WriteLine("Init Post(...)");
		}

		public void AddFlag(Flags flg){
			DatabaseManager.AddPostFlag(PID, flg);
			PostFlags=PostFlags | (int)flg;
		}

		public void RemoveFlag(Flags flg){
			DatabaseManager.RemovePostFlag(PID, flg);
			PostFlags=PostFlags & ~(int)flg;
		}
	}
}
