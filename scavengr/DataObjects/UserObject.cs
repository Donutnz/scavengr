﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using scavengr.Managers;

namespace scavengr.DataObjects {
	/// <summary>
	/// Used for viewing other users. I.E. not the current user. 
	/// Current user is handled by UserModel.
	/// </summary>
	public class UserObject {
		public string UID;
		public string EmailAddr;
		public string Username;
		public float Reputation; //Total of post statuses or avg of post statuses?
		public bool IsVerified; //Might remove.
		public DateTime DateCreated;
		public DateTime? LastLogin; //Can be null.
		public int UserFlags;
		private List<Post> _posts;
		public List<Post> Posts{
			get{
				if(_posts == null){
					_posts = getUserPosts();
				}

				return _posts;
			}
		}

		/// <summary>
		/// User flags. Used for bitwise.
		/// Rank system rolled into this.
		/// </summary>
		public enum Flags{
			NONE, BANNED, MODERATOR, ADMINISTRATOR, DEVELOPER, EDITOR = Flags.MODERATOR | Flags.ADMINISTRATOR | Flags.DEVELOPER
		}

		public UserObject() {
			Console.WriteLine("Init user()");
		}

		public UserObject(string uid, string email, string uname, float rep, bool veri, DateTime created, int uFlg){
			Console.WriteLine("Init user(...)");
			UID = uid;
			EmailAddr = email;
			Username = uname;
			Reputation = rep;
			IsVerified = veri;
			DateCreated = created;
			UserFlags=uFlg;
		}

		private List<Post> getUserPosts(){
			if(String.IsNullOrEmpty(UID)){
				Console.WriteLine("UID null");
				return null;
			}

			string filterQry = "USER_UserID LIKE (@uid)";
			List<MySqlParameter> pms=new List<MySqlParameter>{
				new MySqlParameter("@uid", UID) 
			};

			List<Post> retPosts = DatabaseManager.getPostsBySearch(filterQry, pms);

			return retPosts;
		}
	}
}
