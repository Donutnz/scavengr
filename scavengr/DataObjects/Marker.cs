using System;
using MySql.Data.Types;

namespace scavengr.DataObjects{
	public class Marker{
		public int PostID;
		public string PostTitle;
		public MySqlGeometry coords{get;}
		//Maybe should include MarkID?
		public double X{ //Really need a better method to handle null coord. Null island seems a bad idea.
			get{
				return coords.XCoordinate ?? 0.0;
			}
		}
		public double Y{
			get{
				return coords.YCoordinate ?? 0.0;
			}
		}

		public Marker(){

		}

		public Marker(int pid){
			PostID=pid;
		}

		public Marker(MySqlGeometry geom){
			coords=geom;
		}

		public Marker(int pid, MySqlGeometry geom){
			PostID=pid;
			coords=geom;
		}

		public void SetPostTitle(string title){
			PostTitle=title;
		}
	}
}