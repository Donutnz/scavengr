using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;
using scavengr.DataObjects;
using scavengr.Managers;

namespace scavengr.Models {
	public class SearchModel {
		public int perPage = 5;
		public int curPage = 0;
		private int postCount = 0;
		public int PageCount{
			get{
				return (int)postCount / perPage;
			}
		}
		public int PrevPage{
			get{
				return Math.Max(curPage-1, 0);
			}
		}
		public int NextPage{
			get{
				return Math.Min(curPage + 1, PageCount);
			}
		}
		private string sortCol="";
		private string sortOrder="";

		public SearchModel() {
			Console.WriteLine("Init search model");
			postCount = DatabaseManager.getPostsCount();
		}

		public List<Post> GetPosts(){
			List<Post> posts=null;

			Console.WriteLine("curPage: "+curPage);
			Console.WriteLine("perPage: "+perPage);
			Console.WriteLine("cur*per: "+curPage*perPage);

			posts = DatabaseManager.getPostsSection(curPage * perPage, perPage);

			return posts;
		}

		public List<Post> GetPosts(string qry) {
			List<Post> posts = null;
			List<MySqlParameter> paraCol = new List<MySqlParameter>();
			string srchQry = compileSearchQry(qry, paraCol);

			posts = DatabaseManager.getPostsBySearch(srchQry, paraCol, curPage * perPage, perPage, sortCol +" "+sortOrder);
			return posts;
		}

		public void SetSort(string colStr, string orderStr){
			if(!String.IsNullOrEmpty(colStr)){
				sortCol=getSortCol(colStr);
			}

			if(!String.IsNullOrEmpty(sortCol) && !String.IsNullOrEmpty(orderStr)){
				if(orderStr == "asc"){
					sortOrder="ASC";
				}
				else if(orderStr=="dsc"){
					sortOrder="DESC";
				}
			}
		}

		private string getSortCol(string sortStr) {
			string sortQry="";
				switch (sortStr) {
					case "title":
						sortQry = "Title";
						break;
					case "date":
						sortQry = "Date";
						break;
					case "status":
						sortQry = "Status";
						break;
					default:
						Console.WriteLine("Unknown sort column: " + sortStr);
						break;
				}

			return sortQry;
		}

		//Need to sort out the namespaces as well as incl or excl
		private string compileSearchQry(string tagQry, List<MySqlParameter> paramCol) {
			List<string> rawTags = tagQry.Split(" ").ToList();

			int paramIndex = 0;

			//Not capturing -text values
			Regex nmspaceExp = new Regex(@"(?:-{0,1})(?:(.*)(?<=:)(.*)|(.*))");

			List<string> qryBlks = new List<string>();

			foreach (string str in rawTags) {
				Match mtc = nmspaceExp.Match(str);

				Console.WriteLine("Mtc cnt: " + mtc.Groups.Count);
				foreach (Group grp in mtc.Groups) {
					Console.WriteLine("Grp: " + grp.Value);
				}

				if (str.Contains(':')) {
					switch (mtc.Groups[1].Value) {
						case "author:":
							qryBlks.Add("Username ");
							break;
						case "description:":
							qryBlks.Add("Description ");
							break;
						case "title:":
							qryBlks.Add("Title ");
							break;
						default:
							Console.WriteLine("Unknown tag ns: " + mtc.Value);
							break;
					}
				}
				else {
					qryBlks.Add("Tags ");
				}

				if (str[0] == '-') {
					qryBlks.Add("NOT ");
				}

				qryBlks.Add("LIKE ");

				qryBlks.Add("(@" + paramIndex + ") ");

				//These are different and not just the number. First one has tags. Looking at you, me.
				if (String.IsNullOrEmpty(mtc.Groups[2].Value)) {
					//If tag
					paramCol.Add(new MySqlParameter("@" + paramIndex, "%," + mtc.Groups[3].Value + ",%"));
				}
				else {
					//If other namespace
					paramCol.Add(new MySqlParameter("@" + paramIndex, "%" + mtc.Groups[2].Value + "%"));
				}

				paramIndex++;

				//'AND' or 'OR' feature will go here.
				qryBlks.Add("AND ");
			}

			//Drop the last AND.
			if (qryBlks.Count > 0) {
				qryBlks.RemoveAt(qryBlks.Count - 1);
			}

			string fullQry = String.Join("", qryBlks);

			Console.WriteLine("Full qry: " + fullQry);

			return fullQry;
		}
	}
}