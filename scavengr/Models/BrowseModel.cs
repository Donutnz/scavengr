using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using scavengr.DataObjects;
using scavengr.Managers;

namespace scavengr.Models{
	public class BrowseModel{
		public int perPage=5;
		public int curPage=0;
		private int postCount=0;
		public int PageCount{
			get{
				return (int)postCount / perPage;
			}
		}
		public int PrevPage{
			get{
				return Math.Max(curPage-1, 0);
			}
		}
		public int NextPage{
			get{
				return Math.Min(curPage + 1, PageCount);
			}
		}

		public BrowseModel(){
			Console.WriteLine("Init browse model");
			postCount = DatabaseManager.getPostsCount();
		}

		public List<Post> GetPosts(){
			Console.WriteLine("Set up posts");
			List<Post> posts = DatabaseManager.getPostsSection(curPage * perPage, perPage);

			return posts;
		}

		public string GetMarkers(){
			List<Marker> marks=DatabaseManager.getAllMarkers();

			JArray markers=new JArray();

			foreach(Marker m in marks){
				JObject mj=new JObject();
				mj.Add("PID", m.PostID);
				mj.Add("title", m.PostTitle);
				mj.Add("lng", m.X);
				mj.Add("lat", m.Y);
				
				markers.Add(mj);
			}

			return markers.ToString();
		}
	}
}