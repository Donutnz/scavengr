﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using scavengr.DataObjects;
using scavengr.Managers;

namespace scavengr.Models {
	/// <summary>
	/// Handles the user for the current session.
	/// </summary>
	public class UserModel {
		public string sessionID;
		private string url = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/";
		private string token; //Maybe this shouldn't be stored.
		private string refreshToken;
		public bool isLoggedIn = false;
		private bool isRegistered;

		public string UID;
		public string EmailAddr;
		public string Username;
		public float Reputation;
		public bool Verified;
		public DateTime DateCreated;
		public DateTime LastLogin;
		public int UserFlags;

		public UserModel() {
			Console.WriteLine("User Model created");
		}

		/// <summary>
		/// Handler for adding a new user. Creates Firebase auth record and DB record.
		/// Error types:
		/// 0 = No error
		/// 1 = Firebase error
		/// 2 = Database error
		/// </summary>
		/// <returns>Error type (int) and Error details (string)</returns>
		/// <param name="email">Email</param>
		/// <param name="user">Username</param>
		/// <param name="pass">Password</param>
		public KeyValuePair<int, string> SignUp(string email, string user, string pass) {
			KeyValuePair<int, string> retVal;

			Dictionary<string, string> creds = new Dictionary<string, string> {
				{"email", email},
				{"password", pass},
				{"returnSecureToken", "true"}
			};

			Task<KeyValuePair<HttpStatusCode, JObject>> res = postHTTP("signupNewUser", creds);
			res.Wait();

			if(res.Result.Key == HttpStatusCode.OK) {
				JObject uProps = res.Result.Value;

				token = (string)uProps["idToken"];
				refreshToken = (string)uProps["refreshToken"];
				EmailAddr = (string)uProps["email"];
				UID = (string)uProps["localId"];

				KeyValuePair<bool,int> insSuc=DatabaseManager.InsertUser((string)uProps["localId"], email, user);

				if(insSuc.Key){
					retVal = new KeyValuePair<int, string>(0, "Success");

					DatabaseManager.PopulateUserModel(this);

					isLoggedIn = true;

					Console.WriteLine("New user added successfully");
				}
				else{
					retVal = new KeyValuePair<int, string>(2, insSuc.Value.ToString());
					DeleteAccount();
					Console.WriteLine("Error adding user to DB");
				}
			}
			else {
				retVal = new KeyValuePair<int, string>(1, (string)res.Result.Value["error"]["message"]);
				Console.WriteLine("Error adding user to firebase");
			}

			return retVal;
		}

		/// <summary>
		/// Signs the in.
		/// </summary>
		/// <returns>The in.</returns>
		/// <param name="email">Email.</param>
		/// <param name="pass">Pass.</param>
		public KeyValuePair<bool, string> SignIn(string email, string pass) {
			KeyValuePair<bool, string> retVal = new KeyValuePair<bool, string>(false, "Nothing...");

			Dictionary<string, string> creds = new Dictionary<string, string> {
				{"email", email},
				{"password", pass},
				{"returnSecureToken", "true"}
			};

			Task<KeyValuePair<HttpStatusCode, JObject>> res = postHTTP("verifyPassword", creds);
			res.Wait();

			if(res.Result.Key == HttpStatusCode.OK) {
				JObject uProps = res.Result.Value;

				token = (string)uProps["idToken"];
				refreshToken = (string)uProps["refreshToken"];
				EmailAddr = (string)uProps["email"];
				UID = (string)uProps["localId"];
				isRegistered = (bool)uProps["registered"];

				if(DatabaseManager.PopulateUserModel(this)){
					Console.WriteLine("Popped: " + Username);
					DatabaseManager.UpdateUserLastLog(UID);
				}
				else{
					Console.WriteLine("No populate");
				}

				isLoggedIn = true;

				retVal = new KeyValuePair<bool, string>(true, "Success");
			}
			else {
				retVal = new KeyValuePair<bool, string>(false, (string)res.Result.Value["error"]["message"]);
			}

			return retVal;
		}

		public bool DeleteAccount(){
			if(token == null){
				Console.WriteLine("No token to delete");
				return false;
			}

			Dictionary<string, string> creds = new Dictionary<string, string>{
				{"idToken",token}
			};

			Task<KeyValuePair<HttpStatusCode, JObject>> res = postHTTP("deleteAccount", creds);
			res.Wait();

			if(res.Result.Key == HttpStatusCode.OK){
				Console.WriteLine("Account deleted successfully");
				return true;
			}
			else{
				Console.WriteLine("Account delete failed: " + (string)res.Result.Value["error"]["message"]);
				return false;
			}
		}

		/// <summary>
		/// Changes the email.
		/// </summary>
		/// <returns>The email.</returns>
		/// <param name="oldEmail">Old email.</param>
		/// <param name="pass">Pass.</param>
		/// <param name="newEmail">New email.</param>
		public KeyValuePair<bool, string> ChangeEmail(string oldEmail, string pass, string newEmail){
			bool isSuccess = false;
			string respMsg = "nothing";

			KeyValuePair<bool, string> fbRes=setAccountInfo(oldEmail, pass, "email", newEmail);

			if(fbRes.Key){
				Console.WriteLine("Change email in DB");

				KeyValuePair<bool, int> emDBResp = DatabaseManager.UpdateUserEmail(UID, newEmail);

				if(emDBResp.Key){
					Console.WriteLine("Email DB update success");
					EmailAddr = newEmail;
					isSuccess = true;
					respMsg = "success";
				}
				else{
					Console.WriteLine("Email DB update failed");
					isSuccess = false;
					respMsg = "DB Error: "+emDBResp.Value;
				}
			}
			else{
				Console.WriteLine("Email DB change failed");
				return fbRes;
			}

			return new KeyValuePair<bool, string>(isSuccess, respMsg);
		}

		/// <summary>
		/// Changes the password.
		/// </summary>
		/// <returns>The password.</returns>
		/// <param name="email">Email.</param>
		/// <param name="oldPass">Old pass.</param>
		/// <param name="newPass">New pass.</param>
		public KeyValuePair<bool, string> ChangePassword(string email, string oldPass, string newPass){
			return setAccountInfo(email, oldPass, "password", newPass);
		}

		/// <summary>
		/// Change Firebase user account info.
		/// </summary>
		/// <returns>True if success and message</returns>
		/// <param name="email">Current email</param>
		/// <param name="pass">Current pass</param>
		/// <param name="key">Account info key (e.g. email)</param>
		/// <param name="value">New value for account info</param>
		private KeyValuePair<bool, string> setAccountInfo(string email, string pass, string key, string value) {
			bool isSuccess = false;
			string respMsg = "nothing";

			Dictionary<string, string> creds = new Dictionary<string, string> {
				{"email", email},
				{"password", pass},
				{"returnSecureToken", "true"}
			};

			Task<KeyValuePair<HttpStatusCode, JObject>> res = postHTTP("verifyPassword", creds);
			res.Wait();

			if(res.Result.Key != HttpStatusCode.OK) {
				Console.WriteLine("Auth failed for change: "+key);
				isSuccess = false;
				respMsg = (string)res.Result.Value["error"]["message"];

				return new KeyValuePair<bool, string>(isSuccess, respMsg);
			}

			Console.WriteLine("Auth success for change: "+key);

			Dictionary<string, string> emCreds = new Dictionary<string, string>{
					{"idToken", (string)res.Result.Value["idToken"]},
					{key, value},
					{"returnSecureToken", "true"}
				};

			Task<KeyValuePair<HttpStatusCode, JObject>> emRes = postHTTP("setAccountInfo", emCreds);
			emRes.Wait();

			if(emRes.Result.Key != HttpStatusCode.OK) {
				Console.WriteLine("Change failed: "+key);
				isSuccess = false;
				respMsg = (string)emRes.Result.Value["error"]["message"];

				return new KeyValuePair<bool, string>(isSuccess, respMsg);
			}

			Console.WriteLine("Firebase change success for: "+key);

			return new KeyValuePair<bool, string>(true, "success");
		}

		/// <summary>
		/// Retrieve FireBase user details.
		/// </summary>
		private void getUserDetails() {
			Dictionary<string, string> creds = new Dictionary<string, string>{
				{"idToken", token}
			};

			Task<KeyValuePair<HttpStatusCode, JObject>> res = postHTTP("getAccountInfo", creds);
			res.Wait();

			Console.WriteLine("UD res: " + res.Result.Value);
		}

		private async Task<KeyValuePair<HttpStatusCode, JObject>> postHTTP(string endpoint, IEnumerable<KeyValuePair<string, string>> creds) {
			KeyValuePair<HttpStatusCode, JObject> retVal = new KeyValuePair<HttpStatusCode, JObject>(HttpStatusCode.Ambiguous, JObject.Parse("{\"test\":\"tres\"}"));

			using(HttpClient client = new HttpClient()) {
				string uri = String.Format("{0}{1}?key={2}", url, endpoint, Program.WebKey);

				FormUrlEncodedContent content = new FormUrlEncodedContent(creds);

				//content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				Console.WriteLine("Before POST");
				Task<HttpResponseMessage> respTask= client.PostAsync(uri, content);
				//HttpResponseMessage resp = await client.PostAsync(uri, content);
				//respTask.RunSynchronously();
				Console.WriteLine("After POST");

				HttpResponseMessage resp=await respTask;

				Console.WriteLine("Code: "+resp.StatusCode);
				string result = await resp.Content.ReadAsStringAsync();
				Console.WriteLine("result: " + result);
				//Console.WriteLine("Val: " + await respTask.Result.Content.ReadAsStringAsync());


				retVal = new KeyValuePair<HttpStatusCode, JObject>(resp.StatusCode, JObject.Parse(result));
			}

			return retVal;
		}

		public List<Post> getPosts(){
			List<Post> posts=new List<Post>();

			string filter="UserID LIKE (@uid)";
			List<MySqlParameter> prms=new List<MySqlParameter>{
				new MySqlParameter("@uid", UID)
			};

			posts=DatabaseManager.getPostsBySearch(filter, prms);

			return posts;
		}
	}
}
