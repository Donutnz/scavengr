﻿using System;
using scavengr.Models;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace scavengr.Models {
	/// <summary>
	/// Class to handle posts mysql db
	/// </summary>
	public class PostsModel {
		private MySqlConnection con;
		private string URI;
		private string user = "admin"; //Temp - need to switch to uri or pubkey
		private string pass = "pUJRTLPPGBLPBENOF"; //Temp - need to switch to uri or pubkey
		private string dbName;

		private static PostsModel _instance;
		public static PostsModel Instance {
			get {
				if(_instance == null) {
					_instance = new PostsModel();
				}

				return _instance;
			}
		}

		public PostsModel() {
			

		}

		public bool IsConnected() {
			if(con == null) {
				if(dbName == null || dbName == "") {
					return false;
				}

				connect();
			}

			return true;
		}

		private void connect() {
			if(con == null) {
				string[] conArgs = { URI, dbName, user, pass };
				string conStr = String.Format("Server={0}; database={1}; UID={2}; password={3}", conArgs);
				con = new MySqlConnection(conStr);
				con.Open();
			}
		}

		public MySqlDataReader Query(string qry) {
			MySqlCommand com = new MySqlCommand(qry, con);
			return com.ExecuteReader();
		}

		public void Close() {
			con.Close();
		}
	}
}
