﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace scavengr {
	public class Program {
		/// <summary>
		/// FireBase web key for auth APIs.
		/// </summary>
		public static string WebKey = "AIzaSyBSgR0ggLLuk8UxvSrtY04lJ9iiFV70gUY"; //Should probably move this elsewhere.

		public static void Main(string[] args) {
			BuildWebHost(args).Run();
		}

		public static IWebHost BuildWebHost(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>()
				.Build();
	}
}
