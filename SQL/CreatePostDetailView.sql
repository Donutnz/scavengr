CREATE OR REPLACE VIEW PostDetailView AS
    SELECT 
        POST.*,
        (CASE
            WHEN Username IS NULL THEN 'Anon'
            ELSE Username
        END) AS Username,
        IMAGE.Path,
        MARKER.Coordinate
    FROM
        POST
            LEFT JOIN
        USER ON POST.UserID = USER.UserID
            INNER JOIN
        MARKER ON POST.MarkID = MARKER.MarkID
            INNER JOIN
        IMAGE ON POST.ImgID = IMAGE.ImgID;