-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: 35.240.135.15    Database: Scavengr
-- ------------------------------------------------------
-- Server version	5.7.14-google

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `MARKER`
--

DROP TABLE IF EXISTS `MARKER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MARKER` (
  `MarkID` int(11) NOT NULL AUTO_INCREMENT,
  `Coordinate` point NOT NULL,
  PRIMARY KEY (`MarkID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MARKER`
--

LOCK TABLES `MARKER` WRITE;
/*!40000 ALTER TABLE `MARKER` DISABLE KEYS */;
INSERT INTO `MARKER` VALUES (1,_binary '\0\0\0\0\0\0\0\0\0\0\0\0�B@\0\0\0\0\0\05@'),(2,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(3,_binary '\0\0\0\0\0\0\0Ԛ\��pB�q=\nף\�e@'),(4,_binary '\0\0\0\0\0\0\0q=\nף\�e@Ԛ\��pB�'),(5,_binary '\0\0\0\0\0\0\07\Z�[ ae@?5^�IF�'),(6,_binary '\0\0\0\0\0\0\0\�\�6\Z�\�e@�\nF%ujC�'),(7,_binary '\0\0\0\0\0\0\0\�\�6\Z�\�e@�\nF%ujC�'),(8,_binary '\0\0\0\0\0\0\0\�\�6\Z�\�e@�\nF%ujC�'),(9,_binary '\0\0\0\0\0\0\0\�\�?�߂e@M�\rO7E�'),(10,_binary '\0\0\0\0\0\0\0�:pΈ\�e@�\� �rpB�'),(11,_binary '\0\0\0\0\0\0\0�\Z/\�$�e@l	��g�B�'),(12,_binary '\0\0\0\0\0\0\0EGr��e@�QI��>E�'),(13,_binary '\0\0\0\0\0\0\0q=\nףVe@Ԛ\��p@�'),(14,_binary '\0\0\0\0\0\0\0\�\��S\�e@��\�_v�A�'),(15,_binary '\0\0\0\0\0\0\0\�%\�\�e@\r\�-�B�'),(16,_binary '\0\0\0\0\0\0\0�ʡE�\�e@�\nF%urB�'),(17,_binary '\0\0\0\0\0\0\0�ʡE�\�e@�\nF%urB�'),(18,_binary '\0\0\0\0\0\0\0\�\��S\�e@(��B�'),(19,_binary '\0\0\0\0\0\0\0Gx$\�e@46<\�A�'),(20,_binary '\0\0\0\0\0\0\0U0*��e@2w-!\�A�'),(21,_binary '\0\0\0\0\0\0\0@a\�\�\�e@�G\�z~B�'),(22,_binary '\0\0\0\0\0\0\0t$����e@0L�\nF\�A�'),(23,_binary '\0\0\0\0\0\0\0%��C�e@�Pk�wB�'),(24,_binary '\0\0\0\0\0\0\0��|г\�e@�L�J\�C�'),(25,_binary '\0\0\0\0\0\0\0�Zd\�e@�<,ԚFC�'),(26,_binary '\0\0\0\0\0\0\0�\�v��\�e@ A�c\�=C�');
/*!40000 ALTER TABLE `MARKER` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-15 16:38:58
