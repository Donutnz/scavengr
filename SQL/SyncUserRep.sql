SET @usr='Donutnz'; 

SET @rep=(SELECT SUM(UpVote)-(COUNT(UpVote)-SUM(UpVote)) FROM VOTE WHERE PostID IN (SELECT PostID FROM POST AS p INNER JOIN USER AS u ON p.UserID=u.UserID WHERE Username LIKE (@usr)));

UPDATE USER SET Reputation=@rep WHERE Username LIKE (@usr);
