﻿ISCG7444 - Assignment one vision

Opportunity
The maker/High DIY/garage community has been booming in recent years thanks to the explosion of affordable tools and materials (mostly from China) and the rise of the internet. A notable stumbling point for new makers is sourcing large raw materials (wood, steel, aluminium, etc.) for projects as these cannot easily be ordered. Where to go, what to look for, and how to access are common questions. Buying the materials new from suppliers is usually cost and transport prohibitive. Some people are fortunate in that they have access to offcuts and scraps through their jobs but most do not have this luxury. As such a common place to acquire materials for most people is from skips and other refuse containers. There are two major issues with this method, finding them and contents applicability.
The first issue is finding a skip. Excluding some specific circumstances the primary way of finding a skip is by stumbling upon it. This limits what people can find to their usual routes and general location.
The second issue is contents applicability. As a skip's content is more or less random as is its location the likelihood of someone both finding a skip and being able to make use of all of its contents is quite low. For example a metal working enthusiast might find a skip filled with building waste but only be interested in the corrugated iron roof sections. This is not ideal as the skip also contains large wooden joist sections that a wood worker could put to good use.

Solution
Scavengr is our proposed answer to this situation. The site would allow users to browse, filter, and search skips that have been posted by other users. A post would include a photo, list of contents (by the poster's judgement), and a location for a skip or other refuse container. Posting may require an account and a voting system may be implemented. 

Team Names
-Roman Repolovskii
-Josh Thompson-Brown
